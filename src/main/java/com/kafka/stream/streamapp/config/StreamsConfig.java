package com.kafka.stream.streamapp.config;


import com.kafka.stream.streamapp.stream.GreetingsStreams;
import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding(GreetingsStreams.class)
public class StreamsConfig {
}
