package com.kafka.stream.streamapp.service;

import com.kafka.stream.streamapp.model.Greetings;

public interface IGreetingsService {
    void sendGreeting(Greetings greetings);
}
